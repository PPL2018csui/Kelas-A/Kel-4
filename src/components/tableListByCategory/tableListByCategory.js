import React, { Component } from 'react';
import { InstantSearch, Configure, Index, Pagination } from 'react-instantsearch/dom';
import { connectHits } from 'react-instantsearch/connectors';
import QuickView from '../quickView/quickView';
import ViewButton from '../viewButton/viewButton.js';
import './css/tableListByCategory.css';
/* istanbul ignore next */
const RenderHits = ({ hits }) => (
	<ul className="ais-Hits-list">
		{hits.map(( hit ) => {
			return(
				<li className="ais-Hits-item" key={hit.id}>
					<div className='item'>
						<img src={hit.image} alt='asset' height='120px'/>
					</div>
					<div className='button'>
						<QuickView id={hit.id} category={hit.hcategories.lvl0} name={hit.title} image_general={hit.image} description = {hit.description} />
						<ViewButton id={hit.id} category={hit.hcategories.lvl0} />
					</div>
					<hr />
					<div className='item'>
						<p id={hit.id}><b>{hit.title}</b></p>				
					</div>
				</li>
			);
		})}
	</ul>
);
/* istanbul ignore next */
const ConnectedRender = connectHits(RenderHits);
/* istanbul ignore next */
const Content = ({category}) => (
	<Index indexName='staging_products'>
		<Configure filters={`hcategories.lvl0:"${category}"`} />
		<ConnectedRender />
		<div>
			<br />
			<Pagination showLast padding={2} />
		</div>
	</Index>
);

class TableListByCategory extends Component {
	render() {
		return (
			<InstantSearch 
		        appId='D2VY06YP2A' 
		        apiKey='6014c0a86a26290fa9e3654153a74db4' 
		        indexName='staging_products'
		    >
				<div className='TableListByCategory'>
					<section className='List-item'>
						<Content category={this.props.category}/>
					</section>
				</div>

			</InstantSearch>
		);
	}
}

export default TableListByCategory;