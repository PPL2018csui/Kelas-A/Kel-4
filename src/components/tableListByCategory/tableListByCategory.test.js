import React from 'react';
import ReactDOM from 'react-dom';
import { configure,shallow } from 'enzyme';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });
import toJson from 'enzyme-to-json';
import renderer from 'react-test-renderer';
import TableListByCategory from './tableListByCategory';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<TableListByCategory />, div);
	ReactDOM.unmountComponentAtNode(div);
});

it('gets the local database', () => {
	const db = require('../../file_source/dummy_database.json');
	if(!db){
		return 'undefined';
	} 
});

it('shows the right items based on clicked', () => {
	const wrapper = renderer.create(<TableListByCategory category='Sofa' />).toJSON();
	expect(wrapper).toMatchSnapshot();
});