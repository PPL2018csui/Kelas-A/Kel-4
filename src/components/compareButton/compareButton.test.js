import React from 'react';
import ReactDOM from 'react-dom';
import CompareButton from './compareButton.js';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<CompareButton />, div);
	ReactDOM.unmountComponentAtNode(div);
});
