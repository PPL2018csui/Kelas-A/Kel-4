import React from 'react';
import ReactDOM from 'react-dom';
import { configure,shallow } from 'enzyme';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });
import toJson from 'enzyme-to-json';
import renderer from 'react-test-renderer';
import ErrorPage from './errorPage';

describe('ErrorPage renders correctly', () => {
	it('renders without crashing', () => {
		const div = document.createElement('div');
		ReactDOM.render(<ErrorPage />, div);
		ReactDOM.unmountComponentAtNode(div);
	});
	it('show error when there is no data', () => {
		var database = {
			'products':{}
		};
		var contentKeys = Object.keys(database.products);
		var products = contentKeys.map((t) =>
			<p key={t}>{database.products[t]}</p>
		);
		const wrapper = renderer.create(<ErrorPage dataSize={products.length}/>).toJSON();
		expect(wrapper).toMatchSnapshot();
	});
	it('shows error when page is in 404 page', () => {
		var locationPage='/404';
		const wrapper = renderer.create(<ErrorPage location={locationPage} />).toJSON();
		expect(wrapper).toMatchSnapshot();
	});
});
