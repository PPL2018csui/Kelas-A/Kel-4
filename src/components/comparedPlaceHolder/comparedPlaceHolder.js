import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import './css/comparedPlaceHolder.css';
import whiteSpace from './assets/white.png';

class ComparedPlaceHolder extends Component {
	render() {
		return (
			<BrowserRouter>
				<div className="ComparedPlaceHolder">
					<h3 className="ComparedPlaceHolderTitle">Specification</h3>
					<img src={whiteSpace} alt="white item" height="250"/>
					<div className='ComparisonTable'>
						<table className="ComparedPlaceHolderTable">
							<tbody>
								<tr>
									<td className="comparison-params">Brand</td>
								</tr>
								<tr>
									<td className="comparison-params">Color</td>
								</tr>
								<tr>
									<td className="comparison-params">Material</td>
								</tr>
								<tr>
									<td className="comparison-params">Size</td>
								</tr>
								<tr>
									<td className="comparison-params">Weight</td>
								</tr>
								<tr>
									<td className="comparison-params">Price</td>
								</tr>
							</tbody>
						</table>
					</div>					
				</div>
			</BrowserRouter>
		);
	}
}

export default ComparedPlaceHolder;