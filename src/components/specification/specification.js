import React, { Component } from 'react';
import { InstantSearch, Configure, Index } from 'react-instantsearch/dom';
import { connectHits } from 'react-instantsearch/connectors';
import { BrowserRouter } from 'react-router-dom';
import './css/specification.css';
import _ from 'lodash';

class Specification extends Component {
	render() {
		/* istanbul ignore next */
		const RenderHits = ({ hits }) => (
			<div>
				{hits.map(( hit ) => {
					let material = '';
					let length = -1;
					let width = -1;
					let height = -1;
					_.forEach(hit.attributes, value => {
						/* istanbul ignore next */
						if (value.code ===  'a_material') {
							material = value.value;
						};
						/* istanbul ignore next */
						if (value.code ===  'a_size_panjang') {
							length = value.value;
						};
						/* istanbul ignore next */
						if (value.code ===  'a_size_lebar') {
							width = value.value;
						};
						/* istanbul ignore next */
						if (value.code ===  'a_size_tinggi') {
							height = value.value;
						};
					});

					/* istanbul ignore next */
					function createPrice(price) {
						let value = price.split('.')[0];
						let valString = value.toString();
						let len = valString.length;
						let harga = '';
						var i;
						for (i = len; i > 0; i-=3) {
							/* istanbul ignore next */
							if (i === len) {
								harga = valString.substring(i-3, i)
							} 
							/* istanbul ignore next */
							else {
								harga = valString.substring(i-3, i) + "." + harga;
							}
						}
						/* istanbul ignore next */
						return harga;
					}

					return(
						<div key={hit.id}>
							<h3 className='specificationDetailTitle'>Specification & Detail</h3>
							<table className='specificationTable'>
								<thead><tr className='detailPart'><th colSpan='2'>Detail</th></tr></thead>
								<tbody>
									<tr>
										<td>Brand</td><td className = 'brand'>{hit.brand}</td>
									</tr>
									<tr>
										<td>Color</td><td className = 'detail-color'>{hit.color}</td>
									</tr>
									<tr>
										<td>Material</td><td className = 'detail-material'>{material}</td>
									</tr>
								</tbody>
							</table>
							<table className='specificationTable'> 
								<thead><tr className='specificationPart'><th colSpan='2'>Specification</th></tr></thead>
								<tbody>
									<tr>
										<td>Size</td><td className = 'specification-size'>{length} x {width} x {height} cm</td>
									</tr>
									<tr>
										<td>Weight</td><td className = 'specification-weight'>{hit.weight} kg</td>
									</tr>
									<tr>
										<td>Price</td><td className = 'price'>Rp {createPrice(hit.price)}</td>
									</tr>
								</tbody>
							</table>
						</div>
					);
				}
				)}
			</div>
		);

		const ConnectedRender = connectHits(RenderHits);

		const Content = ({ ids }) => (
		    <Index indexName='staging_products'>
		        <Configure filters={`id=${ids}`}/>
		        <ConnectedRender />
		    </Index>
		);

		return (
			<InstantSearch 
		        appId='D2VY06YP2A' 
		        apiKey='6014c0a86a26290fa9e3654153a74db4' 
		        indexName='staging_products'
		    >
				<BrowserRouter>
					<div className='Specification'>					
						<Content ids={this.props.id} />
					</div>
				</BrowserRouter>
			</InstantSearch>
		);
	}
}

export default Specification;