import React, { Component } from 'react';
import ComparedItem from '../../components/comparedItem/comparedItem';
import { InstantSearch, Pagination, Configure, Index } from 'react-instantsearch/dom';
import { connectHits } from 'react-instantsearch/connectors';
import { BrowserRouter } from 'react-router-dom';
import './css/listOfComparedItem.css';

/* istanbul ignore next */
const display = {
	display: 'block'
};

/* istanbul ignore next */
const hide = {
	display: 'none'
};

class ListOfComparedItem extends Component {
	constructor() {
		super();
		this.handleClick = this.handleClick.bind(this);
		this.state = {
			secondId : 0,
			toggleList: true,
			toggleItem: false,
		};
	}
	/* istanbul ignore next */
	handleClick(e){
		if(e != null) {
			var id = e.currentTarget.id;
			this.setState({
				secondId: id
			});
		}
		this.setState({
			toggleList: !this.state.toggleList
		});
		this.setState({
			toggleItem: !this.state.toggleItem
		});
	}
	render() {
		/* istanbul ignore next */
		const RenderHits = ({ hits }) => (
		<table>
			<thead>
				<tr><td colSpan='2'><h4>Pick one below</h4></td></tr>
			</thead>
			<tbody>
				{hits.map(( hit ) => {
					let itemImage = hit.image;
					let itemName = hit.title;
					let itemId = hit.id;
					return(
						<tr className="item" key={hit.id}>
							<td>
								<img src={itemImage}  alt="Item"/>	
							</td>
							<td>
								<p className='listComparedItem' id={itemId} onClick={this.handleClick.bind(this)}>
									{itemName}
								</p>
							</td>
						</tr>
					);
				}
				)}
			</tbody>
		</table>
		);
		/* istanbul ignore next */
		const ConnectedRender = connectHits(RenderHits);
		/* istanbul ignore next */
		const Content = ({ category }) => (
		    <Index indexName="staging_products">
		        <Configure filters={`hcategories.lvl0:"${category}"`} hitsPerPage={5}/>
		        <ConnectedRender />
		        <Pagination showLast padding={0} className="paddingZero"/>
		        <Pagination showLast padding={1} className="paddingOne"/>
		        <Pagination showLast padding={3} className="paddingThree"/>
		    </Index>
		);
		return (
			<InstantSearch 
		        appId='D2VY06YP2A' 
		        apiKey='6014c0a86a26290fa9e3654153a74db4' 
		        indexName='staging_products'
		    >
				<BrowserRouter>
					<div className="ComparedItem">
						<div className="listComparedItemSection" toggle={this.state.toggleList.toString()} style={this.state.toggleList ? display: hide}>
							<h3 className="ComparedItemTitle">List of item</h3>
							<Content category={this.props.category} />
						</div>
						<div className="comparedItemSection" toggle={this.state.toggleItem.toString()} style={this.state.toggleItem ? display: hide}>
							<ComparedItem item="Second Item" id={this.state.secondId}/>
							<button className='closeButton' onClick={this.handleClick}>Pick other items</button>
						</div>
					</div>
				</BrowserRouter>
			</InstantSearch>
		);
	}	
}
export default ListOfComparedItem;