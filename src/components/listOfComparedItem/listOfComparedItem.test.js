import React from 'react';
import ReactDOM from 'react-dom';
import ListOfComparedItem from './listOfComparedItem';
import { configure,shallow } from 'enzyme';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });
import toJson from 'enzyme-to-json';
import renderer from 'react-test-renderer';


it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<ListOfComparedItem />, div);
	ReactDOM.unmountComponentAtNode(div);
});

it('should match its snapshot', () => {
	const wrapper = renderer.create(<ListOfComparedItem category='Sofa' />).toJSON();
	expect(wrapper).toMatchSnapshot();
});	

it('should open list item when closed button is clicked', () => {
	const wrapper = shallow(<ListOfComparedItem category='Sofa' />);
	const closeButton = wrapper.find('button');
	const comparedItem = wrapper.find('[className="comparedItemSection"]').first();
	
	closeButton.simulate('click');
	expect(comparedItem.props().toggle).toEqual('false');
});


it('should do click event', () => {
	const wrapper = shallow(<ListOfComparedItem category='Sofa' />);
	wrapper.instance().handleClick({currentTarget:{id:3}});
	console.log(wrapper.instance().state.secondId);

	expect(wrapper.instance().state.secondId).toEqual(3);
});

