import React from 'react';
import './css/slider.css';

const RightArrow = (props) => {
	return (
		<div onClick={props.nextSlide} className="nextArrow">
			<button className="right-arrow" aria-hidden="true">&gt;</button>
		</div>
	);
};

export default RightArrow;