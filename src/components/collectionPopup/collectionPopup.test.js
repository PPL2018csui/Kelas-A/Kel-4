import React from 'react';
import ReactDOM from 'react-dom';
import CollectionPopup from './collectionPopup';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<CollectionPopup />, div);
	ReactDOM.unmountComponentAtNode(div);
});
