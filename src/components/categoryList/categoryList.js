import React, { Component } from 'react';
import { InstantSearch, Configure, Index, Pagination } from 'react-instantsearch/dom';
import { connectHits } from 'react-instantsearch/connectors';
import './css/categoryList.css';
import { BrowserRouter, Link } from 'react-router-dom';
import ErrorBoundary from '../errorPage/errorPage';

class CategoryList extends Component {
	render() {
		/* istanbul ignore next */
		const RenderHits = ({ hits }) => (
			<div>
				{hits.map((hit) => {
					var link = 'items/' + hit.hcategories.lvl0;
					return(
						<div>
							<div className='listCategoryText' key={hit.hcategories.lvl0}>
								<button>
									<Link to={link} onClick='window.location.reload();'>
										<div className='item'>
										</div>
										<div className='item'>
											<h2><b>{hit.hcategories.lvl0}</b></h2>
										</div>

									</Link>
								</button>
							</div>

						</div>
					);
				})}
				<div>
					<Pagination showLast/>
				</div>
			</div>
		);
		/* istanbul ignore next */
		const ConnectedRender = connectHits(RenderHits);
		/* istanbul ignore next */
		const Content = () => (
		    <Index indexName='staging_products'>
		        <Configure />
		        <ConnectedRender />
		    </Index>
		);

		return(
			<InstantSearch 
		        appId='D2VY06YP2A' 
		        apiKey='6014c0a86a26290fa9e3654153a74db4' 
		        indexName='staging_products'
		    >
				<ErrorBoundary >
					<BrowserRouter>
						<div className='categoryList'>
							<Content />
						</div>
					</BrowserRouter>
				</ErrorBoundary>
			</InstantSearch>
		);
	}
}

export default CategoryList;