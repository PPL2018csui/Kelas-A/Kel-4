import React, { Component } from 'react';
import { BrowserRouter, Link } from 'react-router-dom';
import './css/quickView.css';

const display = {
	display: 'block'
};

const hide = {
	display: 'none'
};

class QuickView extends Component {
	constructor() {
		super();
		this.handleClick = this.handleClick.bind(this);
		this.state = {
			toggle: false,
		};
	}

	handleClick() {
		this.setState({
			toggle: !this.state.toggle
		});
	}

	getDescriptionSummary(description) {
		var descriptionSummary = '';
		if (description != null){
			descriptionSummary += this.props.description.slice(0,140);
		}
		return descriptionSummary;
	}

	render() {
		var link = '/details/'+ this.props.category+ '/' + this.props.id;
		return (
			<BrowserRouter>
				<div className='QuickView'>
					<button onClick={this.handleClick}>Quick View</button>
					<div className="modal" toggle={this.state.toggle.toString()} style={this.state.toggle ? display : hide}>
						<div className="modal-content">
							<div className="photoModal">
								<img src={this.props.image_general} alt='asset' height='150px'/>
							</div>
							<div className="titleModal">
								<h6>{this.props.name}</h6>
							</div>
							<div className="shortDescription">
								<p>{this.getDescriptionSummary(this.props.description)}... <Link to={link} onClick='window.location.reload();'>Read more</Link></p>
							</div>
						</div>
						<div className="modal-footer">
							<a className="btn" onClick={this.handleClick}>Close</a>
						</div>
					</div>
				</div>
			</BrowserRouter>	
		);
	}
}
export default QuickView;