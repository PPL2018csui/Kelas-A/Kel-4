import React, { Component } from 'react';
import { InstantSearch, Configure, Index } from 'react-instantsearch/dom';
import { connectHits } from 'react-instantsearch/connectors';
import { BrowserRouter } from 'react-router-dom';
import './css/comparedItem.css';
import _ from 'lodash';

class ComparedItem extends Component {
	render() {
		/* istanbul ignore next */
		const RenderHits = ({ hits }) => (
			<div>
				{hits.map((hit) => {
					let material = '';
					let length = -1;
					let width = -1;
					let height = -1;
					_.forEach(hit.attributes, value => {
						if (value.code ===  'a_material') {
							material = value.value;
						};
						if (value.code ===  'a_size_panjang') {
							length = value.value;
						};
						if (value.code ===  'a_size_lebar') {
							width = value.value;
						};
						if (value.code ===  'a_size_tinggi') {
							height = value.value;
						};
					});

					function createPrice(price) {
						let value = price.split('.')[0];
						let valString = value.toString();
						let len = valString.length;
						let harga = '';
						var i;
						for (i = len; i > 0; i-=3) {
							if (i === len) {
								harga = valString.substring(i-3, i)
							} else {
								harga = valString.substring(i-3, i) + "." + harga;
							}
						}
						return harga;
					}
					
					return(
						<div key={hit.id}>
							<div className='ComparisonTable'>
								<img src={hit.image}  alt="Item" height="250"/>
								<table className="ComparedItemTable">
									<tbody>
										<tr>
											<td className = "comparison-value">{hit.color}</td>
										</tr>
										<tr>
											<td className = "comparison-value">{length} x {width} x {height} cm</td>
										</tr>
										<tr>
											<td className = "comparison-value">{hit.weight} kg</td>
										</tr>
										<tr>
											<td className = "comparison-value">Rp {createPrice(hit.price)}</td>
										</tr>
										<tr>
											<td className = "comparison-value">{material}</td>
										</tr>
										<tr>
											<td className = "comparison-value">{hit.title}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					);
				})}
			</div>
		);

		const ConnectedRender = connectHits(RenderHits);

		const Content = ({ ids }) => (
		    <Index indexName='staging_products'>
		        <Configure filters={`id=${ids}`}/>
		        <ConnectedRender />
		    </Index>
		);

		return (
			<InstantSearch 
		        appId='D2VY06YP2A' 
		        apiKey='6014c0a86a26290fa9e3654153a74db4' 
		        indexName='staging_products'
		    >
				<BrowserRouter>
					<div className="ComparedItem">
						<h3 className="ComparedItemTitle">{this.props.item} Detail</h3>
						<Content ids={this.props.id} />
					</div>
				</BrowserRouter>
			</InstantSearch>
		);
	}
}

export default ComparedItem;