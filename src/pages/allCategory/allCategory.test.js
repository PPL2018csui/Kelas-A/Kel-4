import React from 'react';
import ReactDOM from 'react-dom';
import AllCategory from './allCategory';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<AllCategory />, div);
	ReactDOM.unmountComponentAtNode(div);
});