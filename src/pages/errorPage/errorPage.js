import React, { Component } from 'react';
import Footer from '../../components/footer/footer';
import ErrorBoundary from '../../components/errorPage/errorPage';

class ErrorPage extends Component {
	render() {
		return (
			<div className="ErrorBoundary">
				<ErrorBoundary location='/404'/>
				<Footer />
			</div>
		);
	}
}

export default ErrorPage;
