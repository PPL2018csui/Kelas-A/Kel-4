import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import Header from '../../components/header/header';
import Footer from '../../components/footer/footer';
import Compareditem from '../../components/comparedItem/comparedItem';
import ListOfComparedItem from '../../components/listOfComparedItem/listOfComparedItem';
import './css/comparison.css';

class Comparison extends Component {
	constructor(props) {
		super();
		this.state = {
			firstId: props.id,
			secondId: 0,
			category: props.category,
		};	
	}

	render() {
		return (
			<BrowserRouter>
				<div className='comparison'>
					<Header />
					<div className='flex-container'>
						<div className='centerComponent'>
							<div className='compareSpecification'>
								<Compareditem item="First Item" id={this.state.firstId}/>
							</div>
						</div>
						<div className='rightComponent' id='id2'>
							<div className='rightCompareSpecification'>
								<ListOfComparedItem category={this.state.category} />
							</div>
						</div>
					</div>
					<Footer />
				</div>
			</BrowserRouter>
		);
	}
}

export default Comparison;