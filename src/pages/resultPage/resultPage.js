import React, { Component } from 'react';
import Header from '../../components/header/header';
import Footer from '../../components/footer/footer';
import QuickView from '../../components/quickView/quickView';
import ViewButton from '../../components/viewButton/viewButton.js';
import '../../file_source/css/searchBar.css';
import { InstantSearch, Hits, SearchBox, Pagination } from 'react-instantsearch/dom';
import { connectStateResults } from 'react-instantsearch/connectors';
import PropTypes from 'prop-types';
import qs from 'qs';
import './css/resultPage.css';
/* istanbul ignore next */
const updateAfter = 700;
/* istanbul ignore next */
const createURL = state => `?${qs.stringify(state)}`;
/* istanbul ignore next */
const searchStateToUrl = (props, searchState) => searchState ? `${props.location.pathname}${createURL(searchState)}` : '';
/* istanbul ignore next */
const urlToSearchState = location => qs.parse(window.location.href.split('?')[1]);

/* istanbul ignore next */
const Hit = ({hit}) => {
	function createPrice(price) {
		let value = price.split('.')[0];
		let valString = value.toString();
		let len = valString.length;
		let harga = '';
		var i;
		for (i = len; i > 0; i-=3) {
			if (i === len) {
				harga = valString.substring(i-3, i)
			} else {
				harga = valString.substring(i-3, i) + "." + harga;
			}
		}
		return harga;
	}

	return (
		<div>
			<div className='item'>
				<img src={hit.image} alt='asset'/>
			</div>
			<div className='button'>
				<QuickView id={hit.id} category={hit.hcategories.lvl0} name={hit.title} image_general={hit.image} description = {hit.description} />
				<ViewButton id={hit.id} category={hit.hcategories.lvl0} />
			</div>
			<hr />
			<div className='item'>
				<h5 id={hit.id}>Rp {createPrice(hit.price)}</h5>
				<p id={hit.id}><b>{hit.title}</b></p>
			</div>						
		</div>
	);
};
/* istanbul ignore next */
const Content = connectStateResults(({ searchState, searchResults }) => {
	const hasResults = searchResults && searchResults.nbHits !== 0;

	return (
		<div>
			<div hidden={!hasResults}>
				<Hits hitComponent={Hit} />
				<br />
				<Pagination showLast padding={2} />
				<br />
			</div>
			<div hidden={hasResults} className="notFound">
				<div>No results has been found for "{searchState.query}"</div>
			</div>
		</div>
	);
});

class ResultPage extends Component {
	constructor(props) {
    	super(props);

    	this.state = {
    		searchState: urlToSearchState(props.location),
    	};
  	}
  	/* istanbul ignore next */
	componentWillReceiveProps(props) {
		if (props.location !== this.props.location) {
			this.setState({ searchState: urlToSearchState(props.location) });
		}
	}
	/* istanbul ignore next */
	onSearchStateChange = searchState => 
	{
		/* istanbul ignore next */
		clearTimeout(this.debouncedSetState);
		/* istanbul ignore next */
		this.debouncedSetState = setTimeout(() => {
			this.props.history.push(
				searchStateToUrl(this.props, searchState),
				searchState
			);
		}, updateAfter);
		/* istanbul ignore next */
		this.setState({ searchState });
	};

	render() {
		return (
			<div className="ResultPage">
				<Header />
				<InstantSearch 
					appId="D2VY06YP2A" 
					apiKey="6014c0a86a26290fa9e3654153a74db4" 
					indexName="staging_products"
					searchState={this.state.searchState}
			        onSearchStateChange={this.onSearchStateChange}
			        createURL={createURL}
				>
					<br />
					<div id="search-bar">
						<SearchBox translations={{ placeholder: 'Search for products' }} />
					</div>
					<br />
					<div className="result">
						<section className='List-item'>
							<div>
								<Content />
							</div>
						</section>
					</div>
				</InstantSearch>
				<Footer />
			</div>
		);
	}
}
/* istanbul ignore next */
ResultPage.propTypes = {
	history: PropTypes.shape({
		push: PropTypes.func.isRequired,
	}),
	location: PropTypes.object.isRequired,
};

export default ResultPage;
