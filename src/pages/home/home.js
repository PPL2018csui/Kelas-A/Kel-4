import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import Header from '../../components/header/header';
import Footer from '../../components/footer/footer';
import RecommendedCategory from '../../components/recommendedCategory/recommendedCategory';
import RecommendedItem from '../../components/recommendedItem/recommendedItem';
import './css/home.css';

class Home extends Component {
	render() {
		return (
			<BrowserRouter>
			    <div className="Home">
					<Header />
					<div className="containerCategory">
						<RecommendedCategory />
						<RecommendedItem />
					</div>
					<Footer />
				</div>
			</BrowserRouter>
		);
	}
}

export default Home;
